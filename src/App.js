import React from 'react';
import {
  AppBar,
  Toolbar,
  Typography,
  Card,
  CardContent,
  Grid,
  CircularProgress,
  List,
  ListItem,
  ListItemText,
  Divider
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import lightBlue from '@material-ui/core/colors/lightBlue';
import axios from 'axios';
import moment from 'moment-timezone';

const styles = {
  appBar: {
    backgroundColor: lightBlue[400],
  },
  card: {
    margin: 24,
  },
  loader: {
    margin: 50,
  },
  listItem: {
    padding: 24
  }
}

const apiKey = 'b17aae5d34c8a8a56258ce78c364b135'

class App extends React.Component {
  state = {
    loading: true,
    loadingForecast: true,
    today: {},
    forecast: {},
  }
  getTempInDegree(kelvin) {
    return Math.round(kelvin - 273.15)+'˚C'
  }
  renderListItem = (item, key, classes) => {
    return (
      <React.Fragment key={`lisitem-${key}`}>
        <ListItem classes={{ root: classes.listItem }}>
          <ListItemText primary={moment.unix(item.dt).format('D MMM Y, ddd  hh:mma')} secondary={item.weather[0].main} />
          <ListItemText primary={`${this.getTempInDegree(item.main.temp_min)} - ${this.getTempInDegree(item.main.temp_max)}`} />
        </ListItem>
        <Divider />
      </React.Fragment>
    );
  }
  componentDidMount() {
    axios.get('https://api.openweathermap.org/data/2.5/weather', {
      params: {
        id: '1733046',
        appid: apiKey,
      }
    })
      .then(res => {
        // console.log(res.data);
        this.setState({ loading: false, today: res.data });
      })
      .catch(err => console.log(err));

    axios.get('https://api.openweathermap.org/data/2.5/forecast', {
      params: {
        id: '1733046',
        appid: apiKey,
      }
    })
      .then(res => {
        // console.log(res.data);
        this.setState({ loadingForecast: false, forecast: res.data });
      })
      .catch(err => console.log(err));
  }
  render() {
    const { classes } = this.props;
    const { loading, loadingForecast, today, forecast } = this.state;
    if(loading || loadingForecast)
      return (
        <Grid container justify='center' alignItems='center'>
          <CircularProgress classes={{ root: classes.loader }}/>
        </Grid>
      );
    return (
      <div>
        <AppBar classes={{ root: classes.appBar }} position='sticky'>
          <Toolbar>
            <Typography variant="h6">
              Weather App
            </Typography>
          </Toolbar>
        </AppBar>
        <Grid container spacing={24} direction='column' >
          <Grid item xs={12}>
            <Grid container>
              <Grid item md={6} xs={12}>
                <Card classes={{ root: classes.card }}>
                  <CardContent>
                    <Grid container direction='row'>
                      <Grid item xs={12} sm={5} md={6}>
                        <Typography variant='h1'>
                        {this.getTempInDegree(today.main.temp)}
                        </Typography>
                      </Grid>
                      <Grid item xs={12} sm={7} md={6}>
                        <Grid container direction='column'>
                          <Typography variant='body2'>
                            {`${today.name}, ${today.sys.country}`}
                          </Typography>
                          <Typography variant='body2'>
                            {moment.unix(today.dt).tz('Asia/Kuala_Lumpur').format('ddd, D MMM Y hh:mma Z')}
                          </Typography>
                          <Typography variant='body1'>
                            {today.weather[0].main}
                          </Typography>
                        </Grid>
                      </Grid>
                    </Grid>
                  </CardContent>
                </Card>
              </Grid>
            </Grid>
          </Grid>
          <Grid item xs={12}>
            <List>
            {forecast.list.slice(0, 6).map((i, key) => (
              this.renderListItem(i, key, classes)
            ))}
            </List>
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default withStyles(styles)(App);
